FROM alpine:3.9 AS base

FROM base AS build-base
RUN apk add --no-cache curl

FROM build-base AS kubectl
ARG KUBECTL_VERSION
ARG KUBECTL_CHECKSUM
ARG SOURCE=https://dl.k8s.io/v$KUBECTL_VERSION/kubernetes-client-linux-amd64.tar.gz
ARG TARGET=/kubernetes-client.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha512sum "$TARGET"
RUN echo "$KUBECTL_CHECKSUM *$TARGET" | sha512sum -c -
RUN tar -xvf "$TARGET" -C /

FROM build-base AS helm
ARG HELM_VERSION
ARG HELM_CHECKSUM
ARG SOURCE=https://get.helm.sh/helm-v$HELM_VERSION-linux-amd64.tar.gz
ARG TARGET=/helm.tar.gz
RUN curl -fLSs "$SOURCE" -o "$TARGET"
RUN sha256sum "$TARGET"
RUN echo "$HELM_CHECKSUM *$TARGET" | sha256sum -c -
RUN mkdir -p /helm
RUN tar -xvf "$TARGET" -C /helm

FROM build-base AS stage
WORKDIR /stage
ENV PATH=$PATH:/stage/usr/bin
COPY --from=kubectl /kubernetes/client/bin/kubectl ./usr/bin/
# [ht]* is a hack to match helm and tiller, but not fail if tiller is not present. The tests will catch a missing helm binary.
COPY --from=helm /helm/linux-amd64/[ht]* ./usr/bin/

FROM base
# Standard fix so that golang's "netgo" checks /etc/hosts before doing DNS lookups
# See e.g. https://github.com/docker-library/docker/pull/84
RUN [ ! -e /etc/nsswitch.conf ] && echo 'hosts: files dns' > /etc/nsswitch.conf
RUN apk add --no-cache ca-certificates git
COPY --from=stage /stage/ /
